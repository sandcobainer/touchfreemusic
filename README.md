![alt text](assets/Screen Shot 2018-09-03 at 12.16.59 AM.png "Hooking up a Raspberry PI 3B with  Max/MSP over OSC")

## Building music instruments that fit in your phone or laptop.

Implementing hands free or touch free musical instruments with precision and accuracy is a challenging problem in music/audio technology. 
Thanks to the rise of modern machine learning, AI techniques, small instruments have more power than we can imagine. Combine it with the wide range of Digital Audio Workstations, we have a complete instrument that can be played, triggered or mixed live by a musician.

## Real Time Machine Learning:

I attempt to use real time programming, proximity sensing, light sensing and gesture recognition to create a complete instrument that can be used a MIDI trigger, a sine wave theremin, Digital Audio Effects Pedal.

Gesture Recognition with Machine Learning:
Use hand and body gesture recognition by processing video stream from the microcontroller.
- Needs an computer vision algorithm to calculate 3D distance
OR
Use Microsoft Kinect to process video in 3 Dimensions.

### Leveraging Arduino, Max/MSP and Ableton Live.

# Arduino / Raspberry Pi 
A simple microcontroller that can be built into a stomp box or interacted with through Ableton Live installed on Mac/Windows.

Uses simple Python/C++ code snippets to work with basic sensors like
1. Photo Resistors
2. HC SR04 Ultrasonic Rangers, 
   The HC SR04's are fast and give a high precision of measuring distances, almost down to 0.7cm accuracy. But the frame of conctact (around 5 degrees) is very low. For musical instruments, this is a tedious task to always stay in that region. Using PiCamera is a potential solution.
3. LeapMotion Sensor to perform gesture reccognition of hands and fingers
4. PiCamera using OpenCV to calculate the 3D distance of an object from the camera.
   Distance of an object in front of a camera can be calculated by providing the algorithm a precise width of the object and initial distance from the camera. The Computer Vision algorithm generalises pixel width (apparent width) , focal length of camera, distance and real width to build a model that can approximate distance from the camera.
  
Controls for potentiometers, pitch shift, trigger loops/effects can be sent out to Max using OSC 
using either CNMAT Externals in Max? (unsolved design decision?)

# Max / MSP
- Max for Live can be used in Ableton Live to interact with the microcontroller to trigger/toggle controls.
- Max for Live comes bundled with a machine learning library called "ml-lib"  based on the Gesture Recognition Toolkit by Nick Gillian.
- Gesture Recognition serves an essential purpose of building handsfree musical instruments
- Wubu Gesture Recognition library in MAX/MSP

# Ableton Live : The ultimate DAW solution
Ableton Live holds the ultimate control of recording, playing, mixing, mastering an audio track from beginning to end, making it the ultimate Graphical User Interface solution for the frontend of the instrument.

For more details see [TouchFreeMusic](https://github.com/sandcobainer/touchfreemusic/).

### References

- [Arduino](https://www.arduino.cc/)
- [Raspberry Pi](https://www.raspberrypi.org/)
- [Max](https://cycling74.com/products/max/)
- [Ableton Live](https://www.ableton.com/en/)
- [Maxuino](http://www.maxuino.org/)
- [CNMAT/Oscuino](https://github.com/CNMAT/OSC)
- [ml.lib](http://alimomeni.net/ml.lib)

### Support or Contact

Have ideas to collaborate or send me music/memes? Please try to reach me at my [git](https://github.com/sandcobainer) or [email](sandeep.cr007@gmail.com). (Not really busy.)
