import RPi.GPIO as GPIO
import time
import sys
from range_key_dict import RangeKeyDict

class Pitch:
	def __init__(self,total_length,note_length):
		GPIO.setmode(GPIO.BOARD)
		self.PIN_TRIGGER = 7
		self.PIN_ECHO = 11
		self.samp =  10
		GPIO.setup(self.PIN_TRIGGER, GPIO.OUT)
		GPIO.setup(self.PIN_ECHO, GPIO.IN)
		self.summedtimes = 0
		self.pulse_begin = 0; self.pulse_end =  0
		self.avgdist = 0; self.sumdist = 0
		GPIO.output(self.PIN_TRIGGER, GPIO.LOW)
		self._running = True
		self.total_length = total_length
		self.note_length = note_length
		self.keys = {} # RangeKeyDict({(1,20):'hi',(20,40):'hello'})
		MIDI = 48
		for k in range(10,59,2):
			key = {(k,k+2) : MIDI}
			MIDI +=1
			self.keys.update(key)
		self.keys = RangeKeyDict(self.keys)

	def terminate(self):
		self._running = False

	def pitchControl(self):
		self.summedtimes = 0
		try:
			for x in range(0, self.samp):
				GPIO.output(self.PIN_TRIGGER, GPIO.HIGH)
				time.sleep(0.000001)
				GPIO.output(self.PIN_TRIGGER, GPIO.LOW)
				while (GPIO.input(self.PIN_ECHO)==0):
					self.pulse_start_time = time.time();
				while (GPIO.input(self.PIN_ECHO)==1):
					self.pulse_end_time = time.time();
				self.pulse_duration = self.pulse_end_time - self.pulse_start_time
				self.summedtimes  = self.summedtimes + self.pulse_duration

			self.sumdist  = self.summedtimes * 34300 * 0.5
			self.avgdist = round((self.sumdist/self.samp),1)
			print(self.avgdist)
			if (self.avgdist > 10 and self.avgdist < self.total_length):
				print ('Distance : ' + str(self.avgdist) +' cm')
				return self.keys[self.avgdist]
			else :
				print ('Distance measurement error ' + str(self.avgdist))
				return 0
		except Exception as e:
			print(str(e))


